from distribute_setup import use_setuptools
use_setuptools()

from setuptools import setup, find_packages

setup(
    name='birds',
    version='0.1',
    packages=find_packages(),


    # metadata for upload to PyPI
    author='Austin Bingham',
    author_email='austin.bingham@gmail.com',
    description="Birds!",
    license='MIT',
    keywords='game',
    url='http://bitbucket.org/abingham/birds',
    download_url='https://bitbucket.org/abingham/birds/src',
    long_description='Birds',
    zip_safe=True,
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: X11 Applications',
        'Intended Audience :: Developers', # TODO
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Games/Entertainment :: Arcade',
    ],
    platforms='any',
    install_requires=[
        'baker',
        'pygame',
    ],

    entry_points={
        'console_scripts': [
            'birds = birds.app:main',
        ],
    },


)
