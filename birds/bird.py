import os

import pygame

import birds.gameobject
import birds.vec2d


deltas = [0]
for i in xrange(5, 90, 10):
    deltas += [i, -i]

class Bird(birds.gameobject.ImageGameObject):
    def __init__(self,
                 init_position,
                 leader,
                 speed,
                 game,
                 groups=None):
        birds.gameobject.ImageGameObject.__init__(
            self,
            os.path.join(os.path.split(__file__)[0], 'bird.png'),
            init_position=init_position,
            init_direction=0,
            speed=speed,
            game=game,
            groups=groups)

        self.leader = leader
        self.max_speed = speed

    def _colliding(self, pos):
        new_rect = self.rect.move(pos[0] - self.pos[0],
                                  pos[1] - self.pos[1])
        objs = self.game.object_hash[pos]
        for obj in objs:
            if obj is self:
                continue

            if obj.collides(new_rect):
                return True
        return False

    def _update(self, time_passed):
        self.direction = (self.leader.pos - self.pos).normalized()

        speed = self.max_speed
        while speed >= 0:
            for delta in deltas:
                dir = self.direction.rotated(delta)

                displacement = birds.vec2d.vec2d(
                    dir.x * speed * time_passed,
                    dir.y * speed * time_passed)

                if not self._colliding(self.pos + displacement):
                    self.direction = dir
                    self.speed = speed
                    return
            speed -= 0.02
        self.speed = 0
