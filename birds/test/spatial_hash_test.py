from birds.spatial_hash import SpatialHash

import unittest


class Tests(unittest.TestCase):
    def test_hash_point(self):
        h = SpatialHash(cell_size=10)
        x = h.hash_point((15, 23))
        self.assertEqual(x, (1, 2))

    def test_set_item(self):
        h = SpatialHash(cell_size=7)
        obj = "test"
        h[(123, 234)] = obj

    def test_get_item(self):
        h = SpatialHash(cell_size=7)
        obj = "test"
        point = (123, 234)
        h[point] = obj
        self.assertIn(obj, h[point])

    def test_insert_bounding_box(self):
        h = SpatialHash(cell_size=19)
        tl = (12, 234)
        br = (432, 765)
        obj = "test"
        h.insert_bounding_box(tl, br, obj)

        for x in range(tl[0], br[0]):
            for y in range(tl[1], br[1]):
                self.assertIn(obj, h[(x, y)])

if __name__ == '__main__':
    unittest.main()
