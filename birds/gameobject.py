import pygame

import birds.vec2d


class GameObject(pygame.sprite.Sprite):
    def __init__(self,
                 init_position,
                 init_direction,
                 speed,
                 game,
                 groups=None):
        if groups is None:
            groups = []

        super(GameObject, self).__init__(*groups)

        self.game = game
        self.speed = speed
        self.pos = birds.vec2d.vec2d(init_position)
        self.direction = birds.vec2d.vec2d(1, 0)\
            .rotated(init_direction)\
            .normalized()

    def update(self, time_passed):
        self._update(time_passed)

        displacement = birds.vec2d.vec2d(
            self.direction.x * self.speed * time_passed,
            self.direction.y * self.speed * time_passed)


        self.pos += displacement

        self.game.object_hash.remove_object(self)
        self.game.object_hash.insert_bounding_box(self.rect.topleft,
                                                  self.rect.bottomright,
                                                  self)

    def collides(self, rect):
        return self.rect.colliderect(rect)

    def _update(self, time_passed):
        pass


class RadiusGameObject(GameObject):
    def __init__(self,
                 init_position,
                 init_direction,
                 speed,
                 game,
                 radius,
                 groups=None):
        self.radius = radius
        super(RadiusGameObject, self).__init__(init_position,
                                               init_direction,
                                               speed,
                                               game,
                                               groups)

    def collides(self, rect):
        for pt in (rect.topleft,
                   rect.topright,
                   rect.bottomleft,
                   rect.bottomright):
            if (birds.vec2d.vec2d(*pt) - self.pos).get_length() <= self.radius:
                return True
        return False

    @property
    def rect(self):
        return pygame.Rect(
            self.pos.x - self.radius,
            self.pos.y - self.radius,
            self.radius * 2,
            self.radius * 2)


class ImageGameObject(GameObject):
    def __init__(self,
                 img_filename,
                 init_position,
                 init_direction,
                 speed,
                 game,
                 groups=None):
        GameObject.__init__(self,
                            init_position,
                            init_direction,
                            speed,
                            game,
                            groups)

        self.base_image = pygame.image.load(img_filename).convert_alpha()
        self.image = self.base_image

    @property
    def rect(self):
        return self.image.get_rect().move(*self.pos)

    def update(self, time_passed):
        GameObject.update(self, time_passed)

        self.image = pygame.transform.rotate(
            self.base_image, -self.direction.angle)
