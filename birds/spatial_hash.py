class SpatialHash:
    def __init__(self, cell_size):
        self.cell_size = cell_size
        self.hash = {}

    def hash_point(self, point):
        return int(point[0]/self.cell_size), int(point[1]/self.cell_size)

    def __setitem__(self, point, obj):
        self.hash.setdefault(
            self.hash_point(point),
            set()).add(obj)

    def __getitem__(self, point):
        return self.hash.setdefault(
            self.hash_point(point),
            set())

    def insert_bounding_box(self, top_left, bottom_right, obj):
        min_hash = self.hash_point(top_left)
        max_hash = self.hash_point(bottom_right)

        for x_hash in range(min_hash[0], max_hash[0] + 1):
            for y_hash in range(min_hash[1], max_hash[1] + 1):
                self.hash.setdefault((x_hash, y_hash), set()).add(obj)

    def remove_object(self, obj):
        for s in self.hash.values():
            s.discard(obj)
