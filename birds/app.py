import random

import baker

import birds

import birds.bird
import birds.game
import birds.groups
import birds.hash_display
import birds.leader
import birds.obstacle


def bird_collides(bird):
    for obj in birds.groups.objects:
        if obj.collides(bird.rect):
            return True
    return False


def create_bird(game, leader, min_speed, max_speed):
    bird = birds.bird.Bird(
        init_position=(random.randint(0, game.screen_width),
                       random.randint(0, game.screen_height)),
        leader=leader,
        speed=random.randint(min_speed, max_speed) / 100.0 * leader.speed,
        game=game)

    while bird_collides(bird):
        bird.pos = (random.randint(0, game.screen_width),
                    random.randint(0, game.screen_height))

    return bird

@baker.command(
    default=True,
    params={
        'size_x': 'The width of the board. [>0]. Default=1500.',
        'size_y': 'The height of the board. [>0]. Default=900.',
        'count': 'The number of birds. [>=0]. Default=30.',
        'min_speed': 'Minimum bird speed (percentage of leader speed.) [>0]. Default=35.',
        'max_speed': 'Maximum bird speed (percentage of leader speed.) [>=min_speed]. Default=95.',
        'obstacle_count': 'The number of obstacles. [>=0]. Default=30.',
        'leader_speed': 'The speed of the leader. [>0]. Default=0.2.',
        })
def run_game(size_x=1500,
             size_y=900,
             count=30,
             min_speed=35,
             max_speed=95,
             obstacle_count=30,
             leader_speed=0.2):
    if min_speed > max_speed:
        raise ValueError('min-speed must be <= max_speed')
    if size_x < 0 or size_y < 0:
        raise ValueError('Dimensions must be greater than 0.')
    if count < 0:
        raise ValueError('Number of birds must be greater than 0.')
    if obstacle_count < 0:
        raise ValueError('Number of obstacles must be >= 0.')
    if leader_speed < 0:
        raise ValueError('Leader speed must be >= 0.')

    g = birds.game.Game()

    leader = birds.leader.Leader(
        init_position=(200, 200),
        init_direction=39,
        speed=leader_speed,
        game=g,
        groups=(birds.groups.leaders, birds.groups.objects))

    for _ in range(obstacle_count):
        birds.obstacle.Obstacle(
            random.randint(10, 50),
            (random.randint(0, g.screen_width),
             random.randint(0, g.screen_height)),
            g,
            groups=(birds.groups.objects,))

    for _ in range(count):
        bird = create_bird(g, leader, min_speed, max_speed)
        birds.groups.objects.add(bird)

    g.run()

def main():
    baker.run()

if __name__ == '__main__':
    main()
