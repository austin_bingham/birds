import pygame

import birds.gameobject


class Obstacle(birds.gameobject.RadiusGameObject):
    def __init__(self,
                 radius,
                 init_position,
                 game,
                 groups=None):
        self.color = (200, 0, 0)
        birds.gameobject.RadiusGameObject.__init__(
            self,
            init_position,
            0,
            0,
            game,
            radius,
            groups)
        self.image = pygame.Surface((int(self.radius * 2),
                                     int(self.radius * 2)))
        self.image.fill((0,0,0))
        self.image.set_colorkey((0,0,0))
        pygame.draw.circle(
            self.image,
            self.color,
            (radius, radius),
            self.radius,
            0)
