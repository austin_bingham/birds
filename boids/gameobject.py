import pygame

import boids.vec2d


class GameObject(pygame.sprite.Sprite):
    def __init__(self,
                 init_position,
                 init_velocity,
                 repulsion,
                 game,
                 groups=None):
        if groups is None:
            groups = []

        super(GameObject, self).__init__(*groups)

        self.game = game
        self.pos = boids.vec2d.vec2d(init_position)
        self.velocity = boids.vec2d.vec2d(init_velocity)
        self.repulsion = repulsion

    def update(self, time_passed):
        self._update(time_passed)

        self.pos += self.velocity * time_passed / 1000

        # wrap around edges
        if self.pos.x < 0:
            self.pos.x += self.game.screen_width
        elif self.pos.x > self.game.screen_width:
            self.pos.x -= self.game.screen_width

        if self.pos.y < 0:
            self.pos.y += self.game.screen_height
        elif self.pos.y > self.game.screen_height:
            self.pos.y -= self.game.screen_height

        self.game.object_hash.remove_object(self)
        self.game.object_hash.insert_bounding_box(self.rect.topleft,
                                                  self.rect.bottomright,
                                                  self)

    def collides(self, rect):
        return self.rect.colliderect(rect)

    def _update(self, time_passed):
        pass


class RadiusGameObject(GameObject):
    def __init__(self,
                 init_position,
                 init_velocity,
                 game,
                 radius,
                 groups=None):
        self.radius = radius
        super(RadiusGameObject, self).__init__(init_position,
                                               init_velocity,
                                               radius,
                                               game,
                                               groups)

    def collides(self, rect):
        for pt in (rect.topleft,
                   rect.topright,
                   rect.bottomleft,
                   rect.bottomright):
            if (boids.vec2d.vec2d(*pt) - self.pos).get_length() <= self.radius:
                return True
        return False

    @property
    def rect(self):
        return pygame.Rect(
            self.pos.x - self.radius,
            self.pos.y - self.radius,
            self.radius * 2,
            self.radius * 2)


class ImageGameObject(GameObject):
    def __init__(self,
                 img_filename,
                 init_position,
                 init_velocity,
                 game,
                 groups=None):
        self.base_image = pygame.image.load(img_filename).convert_alpha()

        GameObject.__init__(self,
                            init_position=init_position,
                            init_velocity=init_velocity,
                            repulsion= max(*self.base_image.get_rect()) / 2,
                            game=game,
                            groups=groups)

        self.image = self.base_image

    @property
    def rect(self):
        return self.image.get_rect().move(*self.pos)

    def update(self, time_passed):
        GameObject.update(self, time_passed)

        self.image = pygame.transform.rotate(
            self.base_image, -self.velocity.angle)
