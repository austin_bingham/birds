import pygame

import boids.gameobject
from boids.vec2d import vec2d


class Obstacle(boids.gameobject.RadiusGameObject):
    def __init__(self,
                 radius,
                 init_position,
                 game,
                 groups=None):
        self.color = (200, 0, 0)
        boids.gameobject.RadiusGameObject.__init__(
            self,
            init_position,
            vec2d(0, 0),
            game,
            radius,
            groups)
        self.image = pygame.Surface((int(self.radius * 2),
                                     int(self.radius * 2)))
        self.image.fill((0,0,0))
        self.image.set_colorkey((0,0,0))
        pygame.draw.circle(
            self.image,
            self.color,
            (radius, radius),
            radius,
            1)

        pygame.draw.circle(
            self.image,
            (0, 200, 0),
            (radius, radius),
            3, 1)
