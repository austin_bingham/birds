import pygame

import boids.gameobject


class HashDisplay(boids.gameobject.GameObject):
    def __init__(self,
                 sp_hash,
                 game):
        boids.gameobject.GameObject.__init__(
            self,
            (0,0),
            0, 0, 0, game)
        self.sp_hash = sp_hash

    def render(self):
        for pt in self.sp_hash.hash:
            pygame.draw.rect(
                self.game.screen,
                (0, 10, 0),
                pygame.Rect(
                    pt[0] * self.sp_hash.cell_size,
                    pt[1] * self.sp_hash.cell_size,
                    self.sp_hash.cell_size,
                    self.sp_hash.cell_size))
            self.draw_pos = pygame.Rect(
                0, 0, self.game.screen_width, self.game.screen_height)
