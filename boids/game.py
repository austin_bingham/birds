import sys

import pygame

import boids.groups
import boids.spatial_hash
import boids.vec2d


def exit_game():
    sys.exit()


class Game(object):
    def __init__(self):
        self.bg_color = 255, 255, 255
        self.screen_width = 1024
        self.screen_height = 768

        pygame.init()
        self.screen = pygame.display.set_mode(
            (self.screen_width, self.screen_height), 0, 32)
        self.clock = pygame.time.Clock()

        self.object_hash = boids.spatial_hash.SpatialHash(
            screen_size=(self.screen_width, self.screen_height),
            cell_size=20)

    def run(self):
        self.screen.fill(self.bg_color)
        pygame.display.flip()

        # Populate spatial hash
        for obj in boids.groups.objects:
            self.object_hash.insert_bounding_box(obj.rect.topleft,
                                                 obj.rect.bottomright,
                                                 obj)

        while True:
            time_passed = self.clock.tick(50)

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    exit_game()
                elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                    exit_game()
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    for l in boids.groups.leaders:
                        l.direction = (boids.vec2d.vec2d(event.pos) - l.pos).normalized()

            boids.groups.objects.update(time_passed)
            self.screen.fill(self.bg_color)
            rects = boids.groups.objects.draw(self.screen)
            pygame.display.update(rects)
