import os

import boids.gameobject
import boids.groups


class Leader(boids.gameobject.ImageGameObject):
    def __init__(self,
                 init_position,
                 init_direction,
                 speed,
                 game,
                 groups=None):
        boids.gameobject.ImageGameObject.__init__(
            self,
            os.path.join(os.path.split(__file__)[0], 'leader.png'),
            init_position,
            init_direction,
            speed,
            game,
            groups)

    def _update(self, time_passed):

        bounds_rect = self.game.screen.get_rect()

        if self.pos.x < bounds_rect.left:
            self.pos.x = bounds_rect.left
            self.direction.x *= -1
        elif self.pos.x > bounds_rect.right:
            self.pos.x = bounds_rect.right
            self.direction.x *= -1
        elif self.pos.y < bounds_rect.top:
            self.pos.y = bounds_rect.top
            self.direction.y *= -1
        elif self.pos.y > bounds_rect.bottom:
            self.pos.y = bounds_rect.bottom
            self.direction.y *= -1
