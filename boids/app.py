import random

import baker

import boids

import boids.bird
import boids.game
import boids.groups
import boids.hash_display
import boids.leader
import boids.obstacle
from boids.vec2d import vec2d


def bird_collides(bird):
    for obj in boids.groups.objects:
        if obj.collides(bird.rect):
            return True
    return False


def create_bird(game, min_speed, max_speed):
    max_speed = random.randint(min_speed, max_speed)
    speed = random.randint(min_speed, max_speed)
    direction = random.randint(0, 360)

    bird = boids.bird.Bird(
        init_position=(random.randint(0, game.screen_width),
                       random.randint(0, game.screen_height)),
        init_velocity=(vec2d(1, 0) * speed).rotated(direction),
        max_speed=max_speed,
        turn_rate=90,
        game=game)

    return bird

@baker.command(
    default=True,
    params={
        'size_x': 'The width of the board. [>0]. Default=1500.',
        'size_y': 'The height of the board. [>0]. Default=900.',
        'count': 'The number of birds. [>=0]. Default=30.',
        'min_speed': 'Minimum bird speed (percentage of leader speed.) [>0]. Default=35.',
        'max_speed': 'Maximum bird speed (percentage of leader speed.) [>=min_speed]. Default=95.',
        'obstacle_count': 'The number of obstacles. [>=0]. Default=30.',
        'leader_speed': 'The speed of the leader. [>0]. Default=0.2.',
        })
def run_game(size_x=1500,
             size_y=900,
             count=30,
             min_speed=100,
             max_speed=150,
             obstacle_count=30,
             leader_speed=0.2):
    if min_speed > max_speed:
        raise ValueError('min-speed must be <= max_speed')
    if size_x < 0 or size_y < 0:
        raise ValueError('Dimensions must be greater than 0.')
    if count < 0:
        raise ValueError('Number of birds must be greater than 0.')
    if obstacle_count < 0:
        raise ValueError('Number of obstacles must be >= 0.')
    if leader_speed < 0:
        raise ValueError('Leader speed must be >= 0.')

    g = boids.game.Game()

    # leader = boids.leader.Leader(
    #     init_position=(200, 200),
    #     init_direction=39,
    #     speed=leader_speed,
    #     game=g,
    #     groups=(boids.groups.leaders, boids.groups.objects))

    for _ in range(obstacle_count):
        boids.obstacle.Obstacle(
            random.randint(10, 50),
            (random.randint(0, g.screen_width),
             random.randint(0, g.screen_height)),
            g,
            groups=(boids.groups.objects,))

    for _ in range(count):
        bird = create_bird(g, min_speed, max_speed)
        boids.groups.objects.add(bird)

    g.run()

def main():
    baker.run()

if __name__ == '__main__':
    main()
