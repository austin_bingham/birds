import os

import boids.gameobject
from boids.vec2d import vec2d


deltas = [0]
for i in range(5, 90, 10):
    deltas += [i, -i]


class Bird(boids.gameobject.ImageGameObject):
    def __init__(self,
                 init_position,
                 init_velocity,
                 max_speed,
                 turn_rate, # degrees/second
                 game,
                 groups=None):
        boids.gameobject.ImageGameObject.__init__(
            self,
            os.path.join(os.path.split(__file__)[0], 'bird.png'),
            init_position=init_position,
            init_velocity=init_velocity,
            game=game,
            groups=groups)

        self.max_speed = max_speed
        self.turn_rate = turn_rate

    def repulsion_vector(self):
        rslt = vec2d(0, 0)
        nearby = [obj for obj in self.game.object_hash.nearby(self.pos, self.repulsion * 2)
                  if obj is not self]
        for obj in nearby:
            distance = (self.pos - obj.pos).length
            if distance < max(self.repulsion, obj.repulsion) + 10:
                rslt += (self.pos - obj.pos)

        return rslt

    def direction_vector(self, radius):
        # TODO: This is terrible. We should not be using "isinstance", but
        # rather a sprite group for "things I flock with". Same in the
        # next function.
        nearby = [obj for obj in self.game.object_hash.nearby(self.pos, radius)
                  if obj is not self and isinstance(obj, Bird)]
        if len(nearby) > 0:
            return sum(obj.velocity for obj in nearby) / len(nearby)
        else:
            return vec2d(0, 0)

    def centroid_vector(self, radius):
        nearby = [obj for obj in self.game.object_hash.nearby(self.pos, radius)
                  if obj is not self and isinstance(obj, Bird)]

        if len(nearby) > 0:
            return sum(obj.pos for obj in nearby) / len(nearby)
        else:
            return vec2d(0, 0)

    def _update(self, time_passed):
        # TODO: This needs to be configurable.
        LOCAL_RADIUS=20

        # TODO: Weights on vectors should be configurable.
        new_velocity = sum([
            self.velocity,
            self.repulsion_vector(),
            self.direction_vector(LOCAL_RADIUS) * 0.1,
            self.centroid_vector(LOCAL_RADIUS) * 0.1,
        ])

        if new_velocity.length > self.max_speed:
            new_velocity.length = self.max_speed

        # if new_velocity.get_angle_between(self.velocity) > self.turn_rate / 1000:
        #     new_velocity.angle = self.velocity.angle + self.turn_rate / 1000

        self.velocity = new_velocity
