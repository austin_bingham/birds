import functools


class SpatialHash:
    def __init__(self, screen_size, cell_size):
        self.screen_size = screen_size
        self.cell_size = cell_size
        self.cell_count = (self.screen_size[0] / self.cell_size,
                           self.screen_size[1] / self.cell_size)
        self.hash = {}

    def hash_point(self, point):
        return int(point[0]/self.cell_size), int(point[1]/self.cell_size)

    def __setitem__(self, point, obj):
        self.hash.setdefault(
            self.hash_point(point),
            set()).add(obj)

    def __getitem__(self, point):
        return self.hash.setdefault(
            self.hash_point(point),
            set())

    def nearby(self, point, radius):
        """Find objects within a radius of a point.

        This finds *at least* all objects within a radius. More
        realistically, it returns all objects within a bounding rect
        containing ``radius`` around ``point``.
        """

        # Hash the corners of the bounds
        min_hash = self.hash_point((point[0] - radius,
                                    point[1] - radius))

        max_hash = self.hash_point((point[0] + radius,
                                    point[1] + radius))

        # Find all objects in the bounding rect
        rslt = set()
        for x in range(min_hash[0], max_hash[0] + 1):
            while x < 0:
                x += self.cell_count[0]
            while x > self.cell_count[0]:
                x -= self.cell_count[0]

            for y in range(min_hash[1], max_hash[1] + 1):
                while y < 0:
                    y += self.cell_count[1]
                while y > self.cell_count[1]:
                    y -= self.cell_count[1]

                rslt.update(self.hash.get((x, y), set()))
        return rslt

    def insert_bounding_box(self, top_left, bottom_right, obj):
        min_hash = self.hash_point(top_left)
        max_hash = self.hash_point(bottom_right)

        for x_hash in range(min_hash[0], max_hash[0] + 1):
            for y_hash in range(min_hash[1], max_hash[1] + 1):
                self.hash.setdefault((x_hash, y_hash), set()).add(obj)

    def remove_object(self, obj):
        for s in self.hash.values():
            s.discard(obj)
